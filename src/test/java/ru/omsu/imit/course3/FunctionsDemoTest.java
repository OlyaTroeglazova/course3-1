package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.person.Person;
import ru.omsu.imit.course3.person.PersonOptional;
import ru.omsu.imit.course3.person.PersonWithAge;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static ru.omsu.imit.course3.person.Person.getMothersMotherFather;


public class FunctionsDemoTest {

    @Test
    public void splitTest(){
        String s = "Hello world hello world ";
        List<String> res = new ArrayList<>();
        res.add("Hello");
        res.add("world");
        res.add("hello");
        res.add("world");
        Assert.assertEquals((Integer)4, FunctionsDemo.count.apply(FunctionsDemo.split.apply(s)));
        Assert.assertEquals((Integer)4, FunctionsDemo.countMethodReference.apply(FunctionsDemo.splitMethodReference.apply(s)));
    }

    @Test
    public void splitAndCountTest(){
        String s = "Hello world hello world ";
        Assert.assertEquals((Integer)4, FunctionsDemo.splitAndCount1.apply(s));
        Assert.assertEquals((Integer)4, FunctionsDemo.splitAndCount2.apply(s));
    }

    @Test
    public void createTest(){
        String s = "Name1";
        Assert.assertEquals(FunctionsDemo.create.apply(s), new Person(s));
    }

    @Test
    public void maxTest(){
        Assert.assertEquals(FunctionsDemo.max.apply(3543,54321), (Integer)54321);
    }

    @Test
    public void dateTest(){
        Assert.assertTrue(FunctionsDemo.getCurrentDate.get().equals(new Date()));
    }

    @Test
    public void isEvenTest(){
        Assert.assertTrue(FunctionsDemo.isEven.test(54));
    }

    @Test
    public void areEqualTest(){
        Assert.assertTrue(FunctionsDemo.areEqual.test(33,33));
        Assert.assertEquals(FunctionsDemo.areEqual.test(21, 11), false);
    }

    @Test
    public void getMothersMotherFatherTest() throws Exception {
        Person MothersMotherFather = new Person("MothersMotherFather");
        Person p = new Person(new Person(new Person(new Person("MothersMotherMother"), new Person("MothersMotherFather"), "MothersMother"),
                new Person(new Person("MothersFatherMother"), new Person("MothersFatherFather"), "MothersFather"), "Mother"),
                new Person("Father"),
                "Person");
        Assert.assertEquals(MothersMotherFather, getMothersMotherFather(p));
        Assert.assertEquals(null, getMothersMotherFather(new Person("Person")));
    }

    @Test
    public void testFuncMothersMotherFatherOptionals() {
        PersonOptional personOptional = new PersonOptional(new PersonOptional(),
                new PersonOptional(new PersonOptional(), new PersonOptional(new PersonOptional(), new PersonOptional())));
        Assert.assertEquals(new PersonOptional(),PersonOptional.getMothersMotherFatherOptional().apply(personOptional));
    }

    @Test
    public void transformTest(){
        int[] arr1 = {1, 2, 3, 4, 5};
        IntStream stream1 = IntStream.of(arr1);
        FunctionsDemo.transform(stream1, item -> item*item).forEach(System.out::println);
        System.out.println();

        IntStream stream2 = IntStream.of(arr1);
        FunctionsDemo.transformParall(stream2, item -> item*item).forEach(System.out::println);
    }

    @Test
    public void testGetNamesUpper30SortByNameLength() {
        List<PersonWithAge> list = new ArrayList<>();
        list.add(new PersonWithAge("Olga", 20));
        list.add(new PersonWithAge("Oleg", 30));
        list.add(new PersonWithAge("Ivan", 40));
        list.add(new PersonWithAge("Max", 33));
        list.add(new PersonWithAge("Mihail", 32));
        list.add(new PersonWithAge("Alexandr", 22));

        List<String> result = new ArrayList<>();
        result.add("Mihail");
        result.add("Ivan");
        result.add("Max");

        Assert.assertEquals(result,FunctionsDemo.getNamesUpper30SortByNameLength.apply(list));
    }

    @Test
    public void testGetNamesUpper30SortByCountOfSimilar() {
        List<PersonWithAge> list = new ArrayList<>();
        list.add(new PersonWithAge("Olga", 23));
        list.add(new PersonWithAge("Olga", 33));
        list.add(new PersonWithAge("Olga", 100));
        list.add(new PersonWithAge("Ivan", 20));
        list.add(new PersonWithAge("Ivan", 40));
        list.add(new PersonWithAge("Ivan", 50));
        list.add(new PersonWithAge("Mihail", 25));

        List<String> expected = new ArrayList<>();
        expected.add("Olga");
        expected.add("Ivan");

        Assert.assertEquals(expected,FunctionsDemo.getNamesUpper30SortByCountOfSimilar.apply(list));
    }

    @Test
    public void sumTest(){
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i<10; i++){
            list.add(i);
        }
        Assert.assertEquals((Integer)45, FunctionsDemo.sum.apply(list));
    }

    @Test
    public void productTest(){
        List<Integer> list = new ArrayList<>();
        for(int i = 1; i<10; i++){
            list.add(i);
        }
        Assert.assertEquals((Integer)362880, FunctionsDemo.product.apply(list));
    }
}