package ru.omsu.imit.course3;
import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.rectangle.Rectangle;

import java.io.*;

import static java.lang.Math.random;
import static org.junit.Assert.assertEquals;
import static ru.omsu.imit.course3.rectangle.RectangleReadAndWrite.*;

public class RectangleTest {

    Rectangle rectangle;

    public RectangleTest() {
        rectangle = new Rectangle((10 * random()), (10 * random()), (10 * random()), (10 * random()));
    }

    @Test
    public void readAndWriteTest() throws IOException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\RectangleFiles\\rectangle.dat");
        writeRectangleToFile(rectangle, file);

        Rectangle rectangle1 = readRectangleFromFile(file);
        assertEquals(rectangle1.getLeftTop(), rectangle.getLeftTop(), 1E-10);
        assertEquals(rectangle1.getRightTop(), rectangle.getRightTop(), 1E-10);
        assertEquals(rectangle1.getLeftBottom(), rectangle.getLeftBottom(), 1E-10);
        assertEquals(rectangle1.getRightBottom(), rectangle.getRightBottom(), 1E-10);
    }

    @Test
    public void readingRectanglesInReverseOrderTest() throws IOException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\RectangleFiles\\rectangles.dat");
        Rectangle[] rectangles = new Rectangle[5];
        for (int i = 0; i < rectangles.length; i++) {
            rectangles[i] = new Rectangle((10 * random()), (10 * random()), (10 * random()), (10 * random()));
        }

        writeRectanglesArrayToFile(rectangles, file);

        Rectangle[] res = new Rectangle[5];
        int j = 4;
        for (int i = 0; i < 5; i++) {
            res[j] = rectangles[i];
            j--;
        }

        Rectangle[] result = readingRectanglesInReverseOrder(file);
        Assert.assertEquals(res, result);
    }

    @Test
    public void outputRectangleToPrintStreamTest() throws IOException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\RectangleFiles\\rectangles2.txt");
        Rectangle[] rectangles = new Rectangle[5];
        for (int i = 0; i < rectangles.length; i++) {
            rectangles[i] = new Rectangle(1., 2., 3., 4.);
        }
        outputRectangleToPrintStream(rectangles, file);
        Rectangle[] res = readRectangleFromTextFile(file);
        for(int i = 0; i<5; i++){
            Assert.assertEquals(rectangles[i], res[i]);
        }
    }
}
