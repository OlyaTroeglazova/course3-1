package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileTest {
    File file;
    File directory;

    public FileTest() throws IOException {
        file = new File("1.dat");
        file.createNewFile();
        directory = new File("2");
        directory.mkdir();

        File f1 = new File(directory.getAbsolutePath(), "f1.dat");
        f1.createNewFile();
        File f2 = new File(directory.getAbsolutePath(), "f2.dat");
        f2.createNewFile();
        File f3 = new File(directory.getAbsolutePath(), "f3.exe");
        f3.createNewFile();
        File f4 = new File(directory.getAbsolutePath(), "f4.txt");
        f4.createNewFile();
    }

    @Test
    public void createFileTest() throws IOException {
        Assert.assertTrue(file.exists());
        Assert.assertTrue(directory.exists());
    }

    @Test
    public void getFullNameTest() {
        String fullNameFile = "C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\1.dat";
        String fullNameDirectory = "C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\2";
        Assert.assertEquals(file.getAbsolutePath(), fullNameFile);
        Assert.assertEquals(directory.getAbsolutePath(), fullNameDirectory);
    }

    @Test
    public void typeTest() {
        Assert.assertEquals(file.isDirectory(), false);
        Assert.assertEquals(file.isFile(), true);

        Assert.assertEquals(directory.isDirectory(), true);
        Assert.assertEquals(directory.isFile(), false);
    }

    @Test
    public void getFilesListTest() throws IOException {
        String[] listFiles = directory.list();
        String[] actual = {"f1.dat", "f2.dat", "f3.exe", "f4.txt"};
        Assert.assertEquals(listFiles, actual);
    }

    @Test
    public void getFilesListTest2() throws IOException {
        String[] listFiles = directory.list((dir, name) -> name.endsWith("dat"));
        String[] actual = {"f1.dat", "f2.dat"};
        Assert.assertEquals(listFiles, actual);
    }

    @Test
    public void deleteTest() {
        file.delete();
        deleteFile(directory);

        Assert.assertEquals(file.exists(), false);
        Assert.assertEquals(directory.exists(), false);
    }


    public static void deleteFile(File file) {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                deleteFile(f);
            }
        }
        file.delete();
    }
}

