package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static org.junit.Assert.*;
import static ru.omsu.imit.course3.trainee.TraineeReadAndWrite.*;
public class TraineeTest {
    Trainee trainee;

    public TraineeTest() throws TraineeException {
        trainee = new Trainee("Olga", "Troeglazova", 1);
    }

    @Test
    public void setTest() throws TraineeException {
        String name = "Maria";
        trainee.setName(name);
        String surname = "Ivanova";
        trainee.setSurname(surname);
        int mark = 5;
        trainee.setMark(mark);
        assertTrue(trainee.getName().equals(name) &&
                trainee.getSurname().equals(surname) &&
                trainee.getMark() == mark);
    }

    @Test
    public void equalsTest() throws TraineeException {
        Trainee trainee2 = new Trainee("Olga", "Troeglazova", 1);
        assertTrue(trainee.equals(trainee2));
    }

    @Test(expected = TraineeException.class)
    public void setWrongNameTest() throws TraineeException {
        trainee.setName("");
    }

    @Test(expected = TraineeException.class)
    public void setWrongSurnameTest() throws TraineeException {
        String s = new String();
        trainee.setSurname(s);
    }

    @Test(expected = TraineeException.class)
    public void setWrongMarkTest() throws TraineeException {
        trainee.setMark(6);
    }

    @Test(expected = TraineeException.class)
    public void constructorWithWrongParamTest() throws TraineeException {
        Trainee trainee2 = new Trainee("", "", 3);
    }

    @Test
    public void writeTraineeToFileTest() throws IOException, TraineeException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\trainee.txt");
        writeTraineeToTextFile(trainee, file);
        Trainee res = readTraineeFromTextFile(file);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void serializeTraineeTest() throws IOException, ClassNotFoundException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\traineeSer.dat");
        serializeTrainee(trainee, file);
        Trainee res = deserializeTrainee(file);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void serializeTraineeToByteArrayOutputStreamTest() throws IOException, ClassNotFoundException {
        byte[] bytes = serializeTraineeToByteArrayOutputStream(trainee);
        Trainee res = deserializeTraineeFromByteArrayInputStream(bytes);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void writeAndReadTraineeJSONTest() {
        Trainee res = writeAndReadTraineeJSON(trainee);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void writeAndReadTraineeToTextFileJSONTest() throws IOException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\traineeJSON.json");
        writeTraineeToTextFileJSON(trainee, file);
        Trainee res = readTraineeFromTextFileJSON(file);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void readFileBytesWithByteBufferTest() throws IOException, TraineeException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\traineeOneLine.txt");
        Trainee res = readFileBytesWithByteBuffer(file);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void readFileBytesWithMappedByteBufferTest() throws IOException, TraineeException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\traineeOneLine.txt");
        Trainee res = readFileBytesWithMappedByteBuffer(file);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void writeAndReadNumbersFrom0To99Test() throws IOException {
        File file = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\numbersFrom0To99.txt");
        byte[] array = writeAndReadNumbersFrom0To99(file);
        for (int i = 0; i < 100; i++) {
            Assert.assertEquals(array[i], i);
        }
    }

    @Test
    public void getSerializedInstanceInByteBufferTest() throws IOException, ClassNotFoundException {
        Trainee res = getSerializedInstanceInByteBuffer(trainee);
        Assert.assertEquals(trainee, res);
    }

    @Test
    public void pathPathsFilesMethodsTest() throws IOException {
        Path path1 = Paths.get("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\filePath1.txt");
        Path path2 = Paths.get("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\filePath2.txt");
        path1.toFile().createNewFile();
        path2.toFile().createNewFile();
        Assert.assertTrue(Files.exists(path1, LinkOption.NOFOLLOW_LINKS));
        Assert.assertTrue(!Files.isDirectory(path1));
        Assert.assertTrue(!Files.isHidden(path1));
        Assert.assertTrue(Files.isReadable(path1));
        Assert.assertTrue(Files.isWritable(path1));
        Files.copy(path1, path2, StandardCopyOption.REPLACE_EXISTING);
        Files.delete(path1);
    }

    @Test
    public void renameFromDatToBinTest() throws IOException{
        Path directory = Paths.get("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\TraineeFiles\\directory");
        for (File file : directory.toFile().listFiles()) {
            assertTrue(file.getName().endsWith(".bin"));
        }
    }
}