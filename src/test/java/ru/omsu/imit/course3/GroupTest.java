package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.group.Group;
import ru.omsu.imit.course3.group.GroupException;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.util.LinkedList;
import java.util.List;

import static ru.omsu.imit.course3.CollectionsDemo.*;

public class GroupTest {
    Group group;

    public GroupTest() throws TraineeException, GroupException {
        List<Trainee> trainees = new LinkedList<>();
        trainees.add(new Trainee("Ivan", "Ivanov", 3));
        trainees.add(new Trainee("Artem", "Artemov", 4));
        trainees.add(new Trainee("Alexey", "Alexeev", 4));
        trainees.add(new Trainee("Maxim", "Maximov", 3));
        trainees.add(new Trainee("Olga", "Olga", 2));
        trainees.add(new Trainee("Mihail", "Mihailov", 1));
        trainees.add(new Trainee("Oleg", "Olegov", 4));
        trainees.add(new Trainee("Kolya", "Nikolaev", 3));
        group = new Group("601", trainees);
    }

    @Test(expected = GroupException.class)
    public void groupTest() throws GroupException, TraineeException {
        Group g = new Group("401", null);
    }


    @Test
    public void sortingGroupMembersByMarkTest() throws TraineeException, GroupException {
        sortingGroupMembersByMark(group);
        Trainee prev = group.getTrainees().get(0);
        for (Trainee trainee: group.getTrainees()) {
            Assert.assertTrue(prev.getMark()<=trainee.getMark());
            prev = trainee;
        }
    }

    @Test
    public void sortingGroupMembersByNameTest() throws GroupException, TraineeException {
        sortingGroupMembersByName(group);
        Trainee prev = group.getTrainees().get(0);
        for (Trainee trainee: group.getTrainees()) {
            Assert.assertTrue(prev.getName().compareTo(trainee.getName())<=0);
            prev = trainee;
        }
    }

}
