package ru.omsu.imit.course3;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.group.Group;
import ru.omsu.imit.course3.group.GroupException;
import ru.omsu.imit.course3.institute.Institute;
import ru.omsu.imit.course3.institute.InstituteException;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.util.*;

import static ru.omsu.imit.course3.CollectionsDemo.*;
import static ru.omsu.imit.course3.CollectionsDemo.findTraineeByName;

public class CollectionsDemoTest {
    Group group;

    public CollectionsDemoTest() throws TraineeException, GroupException {
        List<Trainee> trainees = Collections.synchronizedList(new LinkedList<>());
        trainees.add(new Trainee("Ivan", "Ivanov", 3));
        trainees.add(new Trainee("Artem", "Artemov", 4));
        trainees.add(new Trainee("Alexey", "Alexeev", 4));
        trainees.add(new Trainee("Maxim", "Maximov", 3));
        trainees.add(new Trainee("Olga", "Olga", 2));
        trainees.add(new Trainee("Mihail", "Mihailov", 1));
        trainees.add(new Trainee("Oleg", "Olegov", 4));
        trainees.add(new Trainee("Kolya", "Nikitin", 3));
        group = new Group("601", trainees);
    }

    @Test
    public void findTraineeByNameTest() throws TraineeException {
        Trainee trainee = findTraineeInGroupByName(group, "Olga");
        Assert.assertEquals(trainee.compareTo(new Trainee("Olga", "Olga", 2)), 1);
        Trainee trainee1 = findTraineeInGroupByName(group, "Nikita");
        Assert.assertTrue(trainee1==null);
    }

    @Test
    public void task9() throws TraineeException {
        Collections.reverse(group.getTrainees());
        Collections.rotate(group.getTrainees(), 2);
        Collections.shuffle(group.getTrainees());
        int maxMark = searchTraineeWithMaxMark(group.getTrainees()).getMark();
        Assert.assertEquals(4, maxMark);
        Trainee trainee = findTraineeByName(group.getTrainees(), "Alexey");
    }

    @Test
    public void measureTimeForListTest(){
        List<Integer> linkedList = Collections.synchronizedList(new LinkedList<>());
        for(int i = 0; i<100000; i++){
            linkedList.add(i);
        }
        System.out.println(measureTimeToFindInList(linkedList));
        List<Integer> arrayList = new ArrayList<>();
        for(int i = 0; i<100000; i++){
            arrayList.add(i);
        }
        System.out.println(measureTimeToFindInList(arrayList));
    }

    @Test
    public void measureTimeForSetTest(){
        List<Integer> arrayList = Collections.synchronizedList(new ArrayList<>());
        Random rnd = new Random();
        for(int i = 0; i<100000; i++){
            arrayList.add(rnd.nextInt(10000));
        }
        System.out.println(measureTimeToFindInList(arrayList));
        Set<Integer> hashSet = new HashSet<>();
        for(int i = 0; i<100000; i++){
            hashSet.add(rnd.nextInt(10000));
        }
        System.out.println(measureTimeToFindInSet(hashSet));
        Set<Integer> treeSet = new TreeSet<>();
        for(int i = 0; i<100000; i++){
            treeSet.add(rnd.nextInt(10000));
        }
        System.out.println(measureTimeToFindInSet(treeSet));
    }



    @Test
    public void findTraineeInHashSetTest() throws TraineeException {
        HashSet<Trainee> traineeSet = new HashSet<>();
        for(Trainee trainee: group.getTrainees()) {
            traineeSet.add(trainee);
        }
        Assert.assertTrue( traineeSet.contains(group.getTrainees().get(1)));
        Assert.assertEquals(false,  traineeSet.contains(new Trainee("a", "b", 2)));
        printSet(traineeSet);
    }

    @Test
    public void findTraineeInTreeSetTest() throws TraineeException {
        Set<Trainee> traineeSet = new TreeSet<>((Trainee o1,Trainee o2)-> o1.getName().compareTo(o2.getName()));
        for(Trainee trainee: group.getTrainees()) {
            traineeSet.add(trainee);
        }
        Assert.assertTrue(traineeSet.contains(group.getTrainees().get(1)));
        Assert.assertEquals(false, traineeSet.contains(new Trainee("a", "b", 2)));
        printSet(traineeSet);
    }

    @Test
    public void findTraineeWithTheSameNameAndADifferentSurnameTest() throws TraineeException {
        Set<Trainee> traineeSet = new TreeSet<>((Trainee o1,Trainee o2)-> o1.getName().compareTo(o2.getName()));
        for(Trainee trainee: group.getTrainees()) {
            traineeSet.add(trainee);
        }
        Assert.assertEquals(traineeSet.contains(new Trainee("Kolya", "Nikolaev", 3)), true);

         printSet(traineeSet);
    }

    @Test
    public void findInstituteByTraineeTest() throws InstituteException {
        Institute institute = new Institute("OmSU", "Omsk");
        HashMap<Trainee, Institute> hashMap = new HashMap<>();
        for(Trainee trainee: group.getTrainees()) {
            hashMap.put(trainee, institute);
        }

        printTraineesInMap(hashMap);
        printMap(hashMap);

        Assert.assertEquals( hashMap.get(group.getTrainees().get(0)).getName(), "OmSU");
    }

    @Test
    public void findTraineeWithTheSameNameAndADifferentSurnameInTreeMapTest() throws InstituteException, TraineeException {
        Institute institute = new Institute("OmSU", "Omsk");
        TreeMap<Trainee, Institute> treeMap = new TreeMap<>((Trainee o1,Trainee o2)-> o1.getName().compareTo(o2.getName()));
        for(Trainee trainee: group.getTrainees()) {
            treeMap.put(trainee, institute);
        }
        Assert.assertEquals(treeMap.containsKey(new Trainee("Oleg", "Ivanov", 4)), true);

        printTraineesInMap(treeMap);
        printMap(treeMap);
    }

    @Test
    public void findAndRemoveIntegerInBitSetTest(){
        BitSet bitSet = new BitSet();
        Random rnd = new Random();
        for(int i = 0; i<100; i++){
            bitSet.set(i);
        }
        int i = rnd.nextInt(100);
        Assert.assertTrue(bitSet.get(i));
        bitSet.clear(i);
        Assert.assertEquals(false, bitSet.get(i));
    }

    @Test
    public void measureTimeForBitSetTest(){
        System.out.println(measureTimeToAddToBitSet(new BitSet()));
        System.out.println(measureTimeToAddToSet(new HashSet<>()));
        System.out.println(measureTimeToAddToSet(new TreeSet<>()));
    }

    @Test
    public void enumSetTest(){
        EnumSet<Color> setAll = EnumSet.allOf(Color.class);
        EnumSet<Color> setBlack = EnumSet.of(Color.BLACK);
        EnumSet<Color> setRange = EnumSet.range(Color.BLUE, Color.WHITE);
        EnumSet<Color> setNone = EnumSet.noneOf(Color.class);
        Assert.assertTrue(setAll.contains(Color.BLACK));
        Assert.assertTrue(setBlack.contains(Color.BLACK));
        Assert.assertEquals(setBlack.contains(Color.BLUE), false);
        Assert.assertTrue(setRange.contains(Color.BLACK));
        Assert.assertEquals(setBlack.contains(Color.RED), false);
        Assert.assertEquals(setNone.contains(Color.BLUE), false);
    }

    @Test
    public void getNonSimilarRowsTest(){
        List<List> bigList = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        List<Integer> list2 = new ArrayList<>();
        list2.add(3);
        list2.add(2);
        list2.add(1);
        List<Integer> list3 = new ArrayList<>();
        list3.add(4);
        list3.add(2);
        list3.add(3);
        bigList.add(list1);
        bigList.add(list2);
        bigList.add(list3);
        Set<List<Integer>> set = getNonSimilarRows(bigList);
    }

}
