package ru.omsu.imit.course3.threads.synchronizedmethods;

public class AddAndRemoveElemThread implements Runnable {
    private ResourceList resource;
    private String mode;

    public AddAndRemoveElemThread(ResourceList resource, String mode) {
        this.resource = resource;
        this.mode = mode;
    }

    @Override
    public void run() {
        if(mode.equals("add")){
            for (int i = 0; i < 10000; i++) {
                resource.add();
            }
        }

        if(mode.equals("remove")){
            for (int i = 0; i < 10000; i++) {
                resource.remove();
            }
        }
    }
}