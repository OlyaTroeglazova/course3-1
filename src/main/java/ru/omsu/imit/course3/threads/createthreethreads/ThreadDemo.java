package ru.omsu.imit.course3.threads.createthreethreads;

public class ThreadDemo {

    public static  void createThreeThreadDemo(){
        Thread firstThread = new Thread(new FirstThread());
        firstThread.start();
        Thread secondThread = new Thread(new SecondThread());
        secondThread.start();
        Thread thirdThread = new Thread(new ThirdThread());
        thirdThread.start();

        try {
            firstThread.join();
            secondThread.join();
            thirdThread.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }

    public static void joinThreadDemo() {
        Thread newThread = new Thread();
        newThread.start();
        try {
            newThread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void currentThreadDemo(){
        Thread currentThread = Thread.currentThread();
        System.out.println("Old name of current thread: "+currentThread.getName());
        currentThread.setName("current");
        System.out.println("New name of current thread: "+currentThread.getName());
        System.out.println("Priority of current thread: "+currentThread.getPriority());
        System.out.println("Group of current thread: "+currentThread.getThreadGroup());
        System.out.println("State of current thread: "+currentThread.getState());
        System.out.println("Thread is running? "+currentThread.isAlive());
        System.out.println("Thread is daemon? "+currentThread.isDaemon());
    }

    public static void main(String[] args) {
        currentThreadDemo();
        joinThreadDemo();
        createThreeThreadDemo();

    }
}
