package ru.omsu.imit.course3.threads.queuetask;

public interface Executable {
    void execute();
}

