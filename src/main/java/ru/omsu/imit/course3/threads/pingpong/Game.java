package ru.omsu.imit.course3.threads.pingpong;

import java.util.concurrent.Semaphore;

public class Game {
    static Semaphore semPing = new Semaphore(1);
    static Semaphore semPong = new Semaphore(0);

    public void ping(){
        try{
            semPing.acquire();
            System.out.println("ping");
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }

        finally {
            semPong.release();
        }
    }

    public void pong(){
        try{
            semPong.acquire();
            System.out.println("pong");
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }

        finally {
            semPing.release();
        }
    }
}
