package ru.omsu.imit.course3.threads.multistagetask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ObserverThread implements Runnable {
    private BlockingQueue<Task> queue;
    private BlockingQueue<String> signalByDeveloper;
    private BlockingQueue<String> signalByExecutor;
    private AtomicInteger devCount;
    private int developerCount;

    public ObserverThread(BlockingQueue<Task> queue, AtomicInteger devCount, int developerCount, BlockingQueue<String> signalByDeveloper, BlockingQueue<String> signalByExecutor) {
        this.queue = queue;
        this.devCount = devCount;
        this.signalByDeveloper = signalByDeveloper;
        this.signalByExecutor = signalByDeveloper;
        this.developerCount = developerCount;
    }

    @Override
    public void run() {
        while (true){
            try{
                if(developerCount==signalByDeveloper.size() || devCount.intValue() == signalByExecutor.size())return;
                else {
                    System.out.println("Observer");
                    for (Task task : queue) {
                        System.out.println("Name: " + task.getName() + "Current stage: " + task.countCurrentStages() + " Count of stages: " + task.getStagesCount());

                    }
                    Thread.sleep(200);
                }
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
    }
}