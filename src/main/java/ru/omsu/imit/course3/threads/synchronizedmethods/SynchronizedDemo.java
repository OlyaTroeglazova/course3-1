package ru.omsu.imit.course3.threads.synchronizedmethods;

public class SynchronizedDemo {
    public static void main(String args[]){
        ResourceList list = new ResourceList();
        String add = "add";
        String remove = "remove";
        Thread addThread = new Thread(new AddAndRemoveElemThread(list, add));
        addThread.start();
        Thread removeThread = new Thread(new AddAndRemoveElemThread(list, remove));
        removeThread.start();

        try {
            addThread.join();
            removeThread.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}

