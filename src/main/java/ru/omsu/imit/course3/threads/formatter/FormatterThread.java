package ru.omsu.imit.course3.threads.formatter;

import java.util.Date;

public class FormatterThread implements Runnable{
    private Formatter formatter;
    private String name;
    private Date date;

    public FormatterThread(Formatter formatter, String name, Date date) {
        this.formatter = formatter;
        this.name = name;
        this.date = date;
    }

    @Override
    public void run() {
        System.out.println(name + " " + formatter.format(date));
    }
}
