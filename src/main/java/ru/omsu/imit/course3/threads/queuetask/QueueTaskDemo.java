package ru.omsu.imit.course3.threads.queuetask;

import com.beust.jcommander.JCommander;

import java.text.ParseException;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class QueueTaskDemo {
    public static void main(String args[]) throws ParseException {
        Args arguments = new Args();
        JCommander jc = JCommander.newBuilder().addObject(arguments).build();
        jc.setProgramName("TaskQueueDemo");
        jc.parse(args);

        final BlockingQueue<Task> queue = new LinkedBlockingDeque<>();

        if (arguments.developers == null || arguments.executors == null) {
            StringBuilder sb = new StringBuilder();
            jc.usage(sb);
            System.out.println(sb.toString());
            return;
        }

        int developerCount = arguments.developers;
        int executorCount = arguments.executors;

        Thread[] developer = new Thread[developerCount];
        Thread[] executor = new Thread[executorCount];


        for (int i = 0; i < developerCount; i++){
            developer[i] = new Thread(new DeveloperThread(queue));
        }
        Arrays.stream(developer).forEach(thread -> thread.start());

        for (int i = 0; i < executorCount; i++){
            executor[i] = new Thread(new ExecutorThread(queue));
        }
        Arrays.stream(executor).forEach(thread -> thread.start());

        try{
            for(Thread thread : developer){
                thread.join();
            }
            for(int i = 0; i < executorCount; i++) {
                queue.put(new Task(0));
            }
            for(Thread thread : executor){
                thread.join();
            }
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}