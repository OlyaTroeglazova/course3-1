package ru.omsu.imit.course3.threads.createthreethreads;

public class FirstThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i <= 1000; i++) {
            System.out.println("I am First Thread " + i);
        }
    }
}