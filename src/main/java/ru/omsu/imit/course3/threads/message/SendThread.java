package ru.omsu.imit.course3.threads.message;

public class SendThread implements Runnable{
    private Message message;
    private Transport transport;

    public SendThread(Message message, Transport transport) {
        this.message = message;
        this.transport = transport;
    }

    @Override
    public void run() {
        transport.send(message);
    }
}
