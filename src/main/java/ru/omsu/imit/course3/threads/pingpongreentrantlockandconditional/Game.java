package ru.omsu.imit.course3.threads.pingpongreentrantlockandconditional;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Game {
    private Lock lock = new ReentrantLock();
    private Condition conPing = lock.newCondition();
    private Condition conPong = lock.newCondition();

    int game = 0;

    public void ping(){
        lock.lock();
        try{
            while (game != 0)
                conPing.await();
            System.out.println("ping");
            game++;
            conPong.signal();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        finally {
            lock.unlock();
        }
    }

    public void pong(){
        lock.lock();
        try{
            while (game != 1)
                conPong.await();
            System.out.println("pong");
            game = 0;
            conPing.signal();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        finally {
            lock.unlock();
        }
    }
}