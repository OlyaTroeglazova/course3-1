package ru.omsu.imit.course3.threads.multistagetask;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class DeveloperThread implements Runnable {
    private BlockingQueue<Task> queue;
    private BlockingQueue<String> signalByDeveloper;
    private AtomicInteger devCount;
    private Random random;
    private String name;

    public DeveloperThread(String name, BlockingQueue<Task> queue, AtomicInteger devCount, BlockingQueue<String> signalByDeveloper) {
        this.queue = queue;
        this.devCount = devCount;
        this.signalByDeveloper = signalByDeveloper;
        this.random = new Random();
        this.name = name;
    }

    @Override
    public void run() {
        for(int i = 0; i < 2; i++){
            try{
                Task task = new Task("MultistageTask № " + i + " By " + this.name, random.nextInt(3)+1);
                queue.put(task);
                System.out.println("Put task " + task.getName()+ " Count of stages: " + task.getStagesCount());
                devCount.incrementAndGet();
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
        try {
            signalByDeveloper.put("Put");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
