package ru.omsu.imit.course3.threads.queuetask;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class Args {
    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = {"-d"}, description = "count of developers")
    public Integer developers;

    @Parameter(names = {"-e"}, description = "count of executors")
    public Integer executors;
}
