package ru.omsu.imit.course3.threads.queuedata;


import java.util.concurrent.BlockingQueue;

public class ReaderThread implements Runnable{
    private BlockingQueue<Data> queue;

    public ReaderThread(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Data data = queue.take();
                if (data.get().length == 0) return;
                System.out.print("Take Data:  ");
                System.out.println(data.toString());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
