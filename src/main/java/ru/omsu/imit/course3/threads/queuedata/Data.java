package ru.omsu.imit.course3.threads.queuedata;

import java.util.Arrays;
import java.util.Random;

public class Data {
    private Random random = new Random();
    private int[] array;

    public Data(int size) {
        array = new int[size];
        for(int i = 0; i < array.length; i++){
            array[i] = random.nextInt(347);
        }
    }

    public int[] get(){
        return array;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}