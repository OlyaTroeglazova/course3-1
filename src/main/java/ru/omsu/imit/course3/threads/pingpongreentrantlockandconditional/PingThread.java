package ru.omsu.imit.course3.threads.pingpongreentrantlockandconditional;

public class PingThread implements Runnable{
    private Game game;

    public PingThread(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        for(int i = 0; i < 10; i++){
            game.ping();
        }
    }
}
