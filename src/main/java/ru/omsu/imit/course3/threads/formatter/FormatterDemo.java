package ru.omsu.imit.course3.threads.formatter;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class FormatterDemo {
    public static void main(String args[]){
        Formatter formatter = new Formatter();
        Thread thread1 = new Thread(new FormatterThread(formatter, "Thread 1", new GregorianCalendar(2018, Calendar.FEBRUARY, 28).getTime()));
        thread1.start();
        Thread thread2 = new Thread(new FormatterThread(formatter, "Thread 2", new GregorianCalendar().getTime()));
        thread2.start();
        Thread thread3 = new Thread(new FormatterThread(formatter, "Thread 3", new GregorianCalendar(1998, Calendar.OCTOBER, 19).getTime()));
        thread3.start();
        Thread thread4 = new Thread(new FormatterThread(formatter, "Thread 4", new GregorianCalendar(2020, Calendar.JULY, 22).getTime()));
        thread4.start();
        Thread thread5 = new Thread(new FormatterThread(formatter, "Thread 5", new GregorianCalendar(2014, Calendar.AUGUST, 30).getTime()));
        thread5.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
            thread5.join();
        } catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}