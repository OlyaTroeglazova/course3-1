package ru.omsu.imit.course3.threads.multistagetask;

import com.beust.jcommander.JCommander;
import ru.omsu.imit.course3.threads.queuetask.Args;

import java.text.ParseException;
import java.util.Arrays;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MultistageTaskDemo {
    public static void main(String args[]) throws ParseException, org.apache.commons.cli.ParseException {
        Args arguments = new Args();
        JCommander jc = JCommander.newBuilder().addObject(arguments).build();
        jc.setProgramName("MultistageTaskDemo");
        jc.parse(args);

        if (arguments.developers == null || arguments.executors == null) {
            StringBuilder sb = new StringBuilder();
            jc.usage(sb);
            System.out.println(sb.toString());
            return;
        }

        int developerCount = arguments.developers;
        int executorCount = arguments.executors;

        BlockingQueue<Task> queue = new LinkedBlockingQueue<>();
        BlockingQueue<String> signalByDeveloper = new LinkedBlockingQueue<>();
        BlockingQueue<String> signalByExecutor = new LinkedBlockingQueue<>();
        AtomicInteger devCount = new AtomicInteger();

        Thread[] developer = new Thread[developerCount];
        Thread[] executor = new Thread[executorCount];

        for (int i = 0; i < developerCount; i++){
            developer[i] = new Thread(new DeveloperThread("Developer " + (i + 1),queue, devCount, signalByDeveloper));
            developer[i].setName("Developer " + (i + 1));
        }
        Arrays.stream(developer).forEach(thread -> thread.start());

        for (int i = 0; i < executorCount; i++){
            executor[i] = new Thread(new ExecutorThread("Executor " + (i + 1),queue, signalByExecutor));
            executor[i].setName("Executor " + (i + 1));
        }
        Arrays.stream(executor).forEach(thread -> thread.start());

        Thread observer = new Thread(new ObserverThread(queue, devCount, developerCount, signalByDeveloper, signalByExecutor));
        observer.start();

        try{
            for(Thread thread : developer) {
                thread.join();
            }
            for(int i = 0; i < executorCount; i++) {
                queue.put(new Task("end", 0));
            }
            for(Thread thread : executor){
                thread.join();
            }
            observer.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        System.out.println("Done.");
    }
}