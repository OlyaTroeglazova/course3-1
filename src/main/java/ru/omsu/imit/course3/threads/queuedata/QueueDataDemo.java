package ru.omsu.imit.course3.threads.queuedata;

import com.beust.jcommander.JCommander;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class QueueDataDemo {
        public static void main(String[] args) {
            Args arguments = new Args();
            JCommander jc = JCommander.newBuilder().addObject(arguments).build();
            jc.setProgramName("DataQueueDemo");
            jc.parse(args);

            final BlockingQueue<Data> queue = new LinkedBlockingDeque<>();

            if (arguments.writers == null || arguments.readers == null) {
                StringBuilder sb = new StringBuilder();
                jc.usage(sb);
                System.out.println(sb.toString());
                return;
            }

            int writerCount = arguments.writers;
            int readerCount = arguments.readers;

            Thread[] writer = new Thread[writerCount];
            Thread[] reader = new Thread[readerCount];

            for (int i = 0; i < writerCount; i++){
                writer[i] = new Thread(new WriterThread(queue));
            }
            Arrays.stream(writer).forEach(thread -> thread.start());

            for (int i = 0; i < readerCount; i++){
                reader[i] = new Thread(new ReaderThread(queue));
            }
            Arrays.stream(reader).forEach(thread -> thread.start());

            try{
                for(Thread thread : writer){
                    thread.join();
                }
                for(int i = 0; i < readerCount; i++) {
                    queue.put(new Data(0));
                }
                for(Thread thread : reader){
                    thread.join();
                }
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
}