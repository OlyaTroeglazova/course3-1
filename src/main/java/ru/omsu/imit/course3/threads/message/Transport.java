package ru.omsu.imit.course3.threads.message;

import java.io.*;
import java.nio.charset.Charset;

public class Transport {
    private File file;

    public Transport(File file) {
        this.file = file;
    }

    public void send(Message message){
        try(FileOutputStream outputStream = new FileOutputStream(file, true);
            OutputStreamWriter osw = new OutputStreamWriter(outputStream)){
            osw.write(message.toString());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
