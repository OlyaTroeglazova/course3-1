package ru.omsu.imit.course3.threads.pingpong;

public class PingPongDemo {
    public static void main(String args[]){
        Game game = new Game();
        Thread pingThread = new Thread(new PingThread(game));
        pingThread.start();
        Thread pongThread = new Thread(new PongThread(game));
        pongThread.start();

        try{
            pingThread.join();
            pongThread.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}
