package ru.omsu.imit.course3.threads.pingpong;

public class PingThread implements Runnable{
    private Game game;

    public PingThread(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        for(;;){
            game.ping();
        }
    }
}