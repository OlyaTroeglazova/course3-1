package ru.omsu.imit.course3.threads.synchronizedmethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ResourceList {
    private List<Integer> list = new ArrayList<>();
    Random random = new Random();

    public List<Integer> getList() {
        return list;
    }

    public synchronized void add(){
        list.add(random.nextInt(10));
    }

    public synchronized void remove(){
        if(list.size() != 0){
            list.remove(random.nextInt(list.size()));
        }
    }
}