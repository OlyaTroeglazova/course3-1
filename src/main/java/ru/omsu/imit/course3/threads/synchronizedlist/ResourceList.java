package ru.omsu.imit.course3.threads.synchronizedlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ResourceList {
    private List<Integer> list = Collections.synchronizedList(new ArrayList<>());
    Random random = new Random();

    public List<Integer> getList() {
        return list;
    }

    public void add(){
        list.add(random.nextInt(10));
    }

    public void remove(){
        if(list.size() != 0){
            list.remove(random.nextInt(list.size()));
        }
    }
}