package ru.omsu.imit.course3.threads.queuedata;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class WriterThread implements Runnable{
    private BlockingQueue<Data> queue;
    private Random random = new Random();

    public WriterThread(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++){
            try {
                queue.put(new Data(random.nextInt(10)));
                System.out.println("Put Data");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
