package ru.omsu.imit.course3.threads.synchronizedreentrantlock;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class AddElemThread implements Runnable{
    private List<Integer> list;
    private Random random = new Random();
    private ReentrantLock lock;

    public AddElemThread(List<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                list.add(random.nextInt(10));
            }finally {
                lock.unlock();
            }
        }
    }
}