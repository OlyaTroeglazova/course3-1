package ru.omsu.imit.course3.threads.synchronizedreentrantlock;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class RemoveElemThread implements Runnable{
    private List<Integer> list;
    private Random random = new Random();
    private ReentrantLock lock;

    public RemoveElemThread(List<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try{
                lock.lock();
                if(list.size() != 0){
                    list.remove(random.nextInt(list.size()));
                }
            }finally {
                lock.unlock();
            }
        }
    }
}