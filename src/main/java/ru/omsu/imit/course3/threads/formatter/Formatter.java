package ru.omsu.imit.course3.threads.formatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    private static ThreadLocal<SimpleDateFormat> threadLocal = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("dd MMMM yyyy  hh.mm");
        }
    };

    public String format(Date date){
        return threadLocal.get().format(date);
    }
}