package ru.omsu.imit.course3.threads.queuetask;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class DeveloperThread implements Runnable {
    private BlockingQueue<Task> queue;
    private Random random = new Random();

    public DeveloperThread(BlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++){
            try {
                queue.put(new Task(random.nextInt(10)));
                System.out.println("Put Task");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}