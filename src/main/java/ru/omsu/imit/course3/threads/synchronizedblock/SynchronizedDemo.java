package ru.omsu.imit.course3.threads.synchronizedblock;

import java.util.ArrayList;
import java.util.List;

public class SynchronizedDemo {
    public static void main(String args[]){
        List<Integer> list = new ArrayList<>();
        Thread add = new Thread(new AddElemThread(list));
        add.start();
        Thread remove = new Thread(new RemoveElemThread(list));
        remove.start();

        try{
            add.join();
            remove.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}
