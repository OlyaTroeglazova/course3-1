package ru.omsu.imit.course3.threads.synchronizedblock;

import java.util.List;
import java.util.Random;

public class AddElemThread implements Runnable{
    private List<Integer> list;
    private Random random = new Random();

    public AddElemThread(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                list.add(random.nextInt(10));
            }
        }
    }
}