package ru.omsu.imit.course3.threads.queuetask;

import java.util.concurrent.BlockingQueue;

public class ExecutorThread implements Runnable {
    private BlockingQueue<Task> queue;

    public ExecutorThread(BlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Task task = queue.take();
                if (task.getNumber() == 0) return;
                System.out.print("Take ");
                task.execute();
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}