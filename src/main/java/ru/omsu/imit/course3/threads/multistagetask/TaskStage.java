package ru.omsu.imit.course3.threads.multistagetask;

import ru.omsu.imit.course3.threads.queuetask.Executable;

public class TaskStage implements Executable {
    private int number;

    public TaskStage(int number) {
        this.number = number;
    }

    @Override
    public void execute() {
        //System.out.println("stage " + number);
    }
}
