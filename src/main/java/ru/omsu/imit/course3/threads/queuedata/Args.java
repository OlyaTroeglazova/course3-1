package ru.omsu.imit.course3.threads.queuedata;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class Args {
    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = {"-w"}, description = "count of writers")
    public Integer writers;

    @Parameter(names = {"-r"}, description = "count of readers")
    public Integer readers;
}
