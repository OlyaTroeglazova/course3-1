package ru.omsu.imit.course3.threads.synchronizedreentrantlock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizedDemo {
    public static void main(String args[]){
        List<Integer> list = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        Thread add = new Thread(new AddElemThread(list, lock));
        add.start();
        Thread remove = new Thread(new RemoveElemThread(list, lock));
        remove.start();

        try{
            add.join();
            remove.join();
        }catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
}