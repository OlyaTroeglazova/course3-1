package ru.omsu.imit.course3.threads.message;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageDemo {
    public static void main(String args[]){
        File emailsFile = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\ThreadFiles\\email.txt");
        File sendFile = new File("C:\\Users\\Я\\Desktop\\универ\\5 семестр\\Программирование серверных приложений\\course3-1\\ThreadFiles\\send.txt");

        List<Message> messages = Message.generationMessage(emailsFile, "Sender@mail.ru", "Subject", "Body ");
        Transport transport = new Transport(sendFile);

        ExecutorService es = Executors.newFixedThreadPool(10);

        for(Message message : messages){
            es.submit(new SendThread(message, transport));
        }
        es.shutdown();
    }
}
