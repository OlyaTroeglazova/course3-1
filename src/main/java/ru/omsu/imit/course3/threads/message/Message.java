package ru.omsu.imit.course3.threads.message;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Message {
    private String emailAddress;
    private String sender;
    private String subject;
    private String body;

    public Message(String emailAddress, String sender, String subject, String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    @Override
    public String toString() {
        return emailAddress + "  " + sender + "  " + subject + "  " + body;
    }

    public static List<Message> generationMessage(File file, String sender, String subject, String body) {
        List<Message> messages = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            while (bufferedReader.ready()) {
                messages.add(new Message(bufferedReader.readLine(), sender, subject, body));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return messages;
    }
}