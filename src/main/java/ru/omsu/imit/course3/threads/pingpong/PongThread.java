package ru.omsu.imit.course3.threads.pingpong;

public class PongThread implements Runnable{
    private Game game;

    public PongThread(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        for(;;){
            game.pong();
        }
    }
}