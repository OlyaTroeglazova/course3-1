package ru.omsu.imit.course3.threads.multistagetask;

import java.util.concurrent.BlockingQueue;

public class ExecutorThread implements Runnable {
    private BlockingQueue<Task> queue;
    private String name;
    private BlockingQueue<String> signalByExecutor;


    public ExecutorThread(String name,BlockingQueue<Task> queue, BlockingQueue<String> signalByExecutor) {
        this.queue = queue;
        this.name = name;
        this.signalByExecutor = signalByExecutor;
    }

    @Override
    public void run() {
        while (true){
            try {
                Task task = queue.take();
                if(task.getStagesCount()==0) return;

                task.executeStage();
                System.out.println("Execute stage " + task.countCurrentStages() + " In " + task.getName());
                if(task.isTaskEnd()){
                    signalByExecutor.put("Done");
                    System.out.println(task.getName() + "is done");
                }else{
                    queue.put(task);
                }
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
    }
}