package ru.omsu.imit.course3.threads.synchronizedblock;

import java.util.List;
import java.util.Random;

public class RemoveElemThread implements Runnable{
    private List<Integer> list;
    private Random random = new Random();

    public RemoveElemThread(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                if(list.size() != 0){
                    list.remove(random.nextInt(list.size()));
                }
            }
        }
    }
}