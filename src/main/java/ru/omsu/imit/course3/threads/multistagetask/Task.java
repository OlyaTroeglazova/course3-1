package ru.omsu.imit.course3.threads.multistagetask;

import ru.omsu.imit.course3.threads.queuetask.Executable;

import java.util.ArrayList;
import java.util.List;

public class Task {
    private List<Executable> stagesList;
    private String name;
    private int stagesCount;

    public Task(String name, int stagesCount) {
        this.name = name;
        this.stagesCount = stagesCount;
        stagesList = new ArrayList<>();
        for(int i = 0; i < stagesCount; i++){
            stagesList.add(new TaskStage(i));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStagesCount() {
        return stagesCount;
    }

    public boolean isTaskEnd(){
        return stagesList.isEmpty();
    }

    public void executeStage(){
        if(stagesCount != 0) {
             stagesList.remove(0);
        }
    }

    public int countCurrentStages(){
        return stagesList.size();
    }
}