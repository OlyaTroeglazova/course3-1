package ru.omsu.imit.course3.institute;

public class Institute {
    private String name;
    private String city;

    public Institute(String name, String city) throws InstituteException {
        this.setName(name);
        this.setCity(city);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InstituteException {
        if(name == null || name.length() == 0){
            throw new InstituteException(InstituteErrorCodes.INCORRECT_NAME.getErrorString());
        }
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws InstituteException {
        if(city == null || city.length() == 0){
            throw new InstituteException(InstituteErrorCodes.INCORRECT_CITY.getErrorString());
        }
        this.city = city;
    }

    @Override
    public String toString() {
        return "Institute{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
