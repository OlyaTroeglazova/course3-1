package ru.omsu.imit.course3.institute;

public enum  InstituteErrorCodes {
    INCORRECT_NAME("Incorrect name"),
    INCORRECT_CITY("Incorrect city");

    private final String errorString;

    private InstituteErrorCodes(String errorString){
        this.errorString = errorString;
    }

    public String getErrorString(){
        return errorString;
    }
}
