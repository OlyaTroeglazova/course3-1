package ru.omsu.imit.course3.institute;

public class InstituteException extends Exception {
    public InstituteException(){
        super();
    }

    public InstituteException(String message){
        super(message);
    }
}
