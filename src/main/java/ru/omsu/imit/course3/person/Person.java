package ru.omsu.imit.course3.person;

public class Person {
    private String name;
    private Person mother;
    private Person father;

    public Person(String s){
        this.name = s;
    }

    public Person(Person mother, Person father, String name) throws Exception {
        if(mother==null||father==null){
            throw new PersonException();
        }
        this.name = name;
        this.mother = mother;
        this.father = father;
    }

    public static Person getMothersMotherFather(Person p){
        if (p == null || p.mother == null ||
                p.mother.mother == null) {
            return null;
        }
        return p.mother.mother.father;
    }

    public Person getMother() {
        return mother;
    }

    public Person getFather() {
        return father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        return father != null ? father.equals(person.father) : person.father == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        return result;
    }

    public class PersonException extends Exception{
        public PersonException(){
            super();
        }
    }

}
