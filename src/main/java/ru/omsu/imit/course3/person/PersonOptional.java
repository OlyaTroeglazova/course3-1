package ru.omsu.imit.course3.person;

import java.util.Optional;
import java.util.function.Function;

public class PersonOptional {
    private Optional<PersonOptional> father;
    private Optional<PersonOptional> mother;

    public PersonOptional() {
    }

    public PersonOptional(PersonOptional father, PersonOptional mother) {
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<PersonOptional> getFather() {
        return father;
    }

    public Optional<PersonOptional> getMother() {
        return mother;
    }

    public static final Function<PersonOptional, PersonOptional> getMothersMotherFatherOptional() {
        return person -> person == null ? null : person.getMother().flatMap(PersonOptional::getMother)
                .flatMap(PersonOptional::getFather).orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonOptional that = (PersonOptional) o;

        if (father != null ? !father.equals(that.father) : that.father != null) return false;
        return mother != null ? mother.equals(that.mother) : that.mother == null;
    }

    @Override
    public int hashCode() {
        int result = father != null ? father.hashCode() : 0;
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        return result;
    }
}