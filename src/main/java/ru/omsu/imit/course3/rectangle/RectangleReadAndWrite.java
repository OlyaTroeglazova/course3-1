package ru.omsu.imit.course3.rectangle;

import java.io.*;

public class RectangleReadAndWrite {

    public static void writeRectangleToFile(Rectangle rectangle, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file);
             DataOutputStream dos = new DataOutputStream(fos)) {
            dos.writeDouble(rectangle.getLeftTop());
            dos.writeDouble(rectangle.getRightTop());
            dos.writeDouble(rectangle.getLeftBottom());
            dos.writeDouble(rectangle.getRightBottom());
        }
    }

    public static Rectangle readRectangleFromFile(File file) throws IOException {
        Rectangle rectangleResult;
        try (FileInputStream fis = new FileInputStream(file)) {
            DataInputStream dis = new DataInputStream(fis);
            rectangleResult = new Rectangle(dis.readDouble(), dis.readDouble(), dis.readDouble(), dis.readDouble());
        }
        return rectangleResult;
    }

    public static void writeRectanglesArrayToFile(Rectangle[] rectangles, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file);
             DataOutputStream dos = new DataOutputStream(fos)) {
            for (int i = 0; i < rectangles.length; i++) {
                dos.writeDouble(rectangles[i].getLeftTop());
                dos.writeDouble(rectangles[i].getRightTop());
                dos.writeDouble(rectangles[i].getLeftBottom());
                dos.writeDouble(rectangles[i].getRightBottom());
            }
        }

    }

    public static Rectangle[] readingRectanglesInReverseOrder(File file) throws IOException {
        Rectangle[] rectanglesResult = new Rectangle[5];
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            int j = 0;
            for (int i = rectanglesResult.length - 1; i >= 0; i--) {
                raf.seek(i * 32);
                double leftTop = raf.readDouble();
                double rightTop = raf.readDouble();
                double leftBottom = raf.readDouble();
                double rightBottom = raf.readDouble();
                rectanglesResult[j] = new Rectangle(leftTop, rightTop, leftBottom, rightBottom);
                j++;
            }
        }
        return rectanglesResult;
    }

    public static void outputRectangleToPrintStream(Rectangle[] rectangles, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file);
             PrintStream printStream = new PrintStream(fos)) {
            for (int i = 0; i < rectangles.length; i++) {
                printStream.println(rectangles[i].getLeftTop());
                printStream.println(rectangles[i].getRightTop());
                printStream.println(rectangles[i].getLeftBottom());
                printStream.println(rectangles[i].getRightBottom());
            }
        }
    }

    public static Rectangle[] readRectangleFromTextFile(File file) throws IOException {
        Rectangle[] rectangles = new Rectangle[5];
        try(FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
            for (int i = 0; i < 5; i++) {
                rectangles[i] = new Rectangle(Double.parseDouble(br.readLine()), Double.parseDouble(br.readLine()), Double.parseDouble(br.readLine()), Double.parseDouble(br.readLine()));
            }
        }
        return rectangles;
    }
}
