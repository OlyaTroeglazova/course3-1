package ru.omsu.imit.course3.rectangle;

public class Rectangle {

    private double leftTop;
    private double rightTop;
    private double leftBottom;
    private double rightBottom;

    public Rectangle(double leftTop, double rightTop, double leftBottom, double rightBottom){
        this.leftTop = leftTop;
        this.rightTop = rightTop;
        this.leftBottom = leftBottom;
        this.rightBottom = rightBottom;
    }

    public double getLeftTop() {
        return leftTop;
    }

    public void setLeftTop(double leftTop) {
        this.leftTop = leftTop;
    }

    public double getRightTop() {
        return rightTop;
    }

    public void setRightTop(double rightTop) {
        this.rightTop = rightTop;
    }

    public double getLeftBottom() {
        return leftBottom;
    }

    public void setLeftBottom(double leftBottom) {
        this.leftBottom = leftBottom;
    }

    public double getRightBottom() {
        return rightBottom;
    }

    public void setRightBottom(double rightBottom) {
        this.rightBottom = rightBottom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.leftTop, leftTop) != 0) return false;
        if (Double.compare(rectangle.rightTop, rightTop) != 0) return false;
        if (Double.compare(rectangle.leftBottom, leftBottom) != 0) return false;
        return Double.compare(rectangle.rightBottom, rightBottom) == 0;
    }

}