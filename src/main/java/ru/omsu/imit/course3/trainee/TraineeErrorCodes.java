package ru.omsu.imit.course3.trainee;

public enum  TraineeErrorCodes {
    INCORRECT_NAME("Incorrect name"),
    INCORRECT_SURNAME("Incorrect surname"),
    INCORRECT_MARK("Incorrect mark");

    private final String errorString;

    private TraineeErrorCodes(String errorString){
        this.errorString = errorString;
    }

    public String getErrorString(){
        return errorString;
    }
}
