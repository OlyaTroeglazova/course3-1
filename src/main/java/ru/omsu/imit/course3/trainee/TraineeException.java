package ru.omsu.imit.course3.trainee;

public class TraineeException extends Exception{
    public TraineeException(){
        super();
    }

    public TraineeException(String message){
        super(message);
    }
}
