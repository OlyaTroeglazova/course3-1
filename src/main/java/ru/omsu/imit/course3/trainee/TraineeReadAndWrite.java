package ru.omsu.imit.course3.trainee;


import com.google.gson.Gson;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class TraineeReadAndWrite {
    private static final int SIZE = 100;

    public static void writeTraineeToTextFile(Trainee trainee, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file);
             PrintStream printStream = new PrintStream(fos)) {
            printStream.println(trainee.getName());
            printStream.println(trainee.getSurname());
            printStream.println(trainee.getMark());
        }
    }

    public static Trainee readTraineeFromTextFile(File file) throws IOException, TraineeException {
        Trainee trainee;
        try (FileInputStream fis = new FileInputStream(file);
             BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
            trainee = new Trainee(br.readLine(), br.readLine(), Integer.parseInt(br.readLine()));
        }
        return trainee;
    }

    public static void writeTraineeToOneLineToTextFile(Trainee trainee, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file);
             PrintStream printStream = new PrintStream(fos)) {
            printStream.print(trainee.getName());
            printStream.print(" ");
            printStream.print(trainee.getSurname());
            printStream.print(" ");
            printStream.print(trainee.getMark());
        }
    }

    public static Trainee readTraineeFromOneLineInTextFile(File file) throws IOException, TraineeException {
        Trainee trainee;
        try (FileInputStream fis = new FileInputStream(file);
             BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
            String[] string = br.readLine().split(" ");
            trainee = new Trainee(string[0], string[1], Integer.parseInt(string[2]));
        }
        return trainee;
    }

    public static void serializeTrainee(Trainee trainee, File file) throws IOException {
        try (FileOutputStream fis = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fis)) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeTrainee(File file) throws IOException, ClassNotFoundException {
        Trainee trainee;
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            trainee = (Trainee) ois.readObject();
        }
        return trainee;
    }

    public static byte[] serializeTraineeToByteArrayOutputStream(Trainee trainee) throws IOException {
        byte[] bytes = null;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(trainee);
            bytes = bos.toByteArray();
        }
        return bytes;
    }

    public static Trainee deserializeTraineeFromByteArrayInputStream(byte[] bytes) throws IOException, ClassNotFoundException {
        Trainee trainee;
        try (ByteArrayInputStream bos = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bos)) {
            trainee = (Trainee) ois.readObject();
        }
        return trainee;
    }

    public static Trainee writeAndReadTraineeJSON(Trainee trainee) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(trainee);

        Trainee res = gson.fromJson(jsonString, Trainee.class);
        return res;
    }

    public static void writeTraineeToTextFileJSON(Trainee trainee, File file) throws IOException {
        Gson gson = new Gson();
        try (FileWriter fileWriter = new FileWriter(file)) {
            gson.toJson(trainee, fileWriter);
        }
    }

    public static Trainee readTraineeFromTextFileJSON(File file) throws IOException {
        Gson gson = new Gson();
        Trainee res;
        try (FileReader fileReader = new FileReader(file)) {
            res = gson.fromJson(fileReader, Trainee.class);
        }
        return res;
    }

    public static Trainee readFileBytesWithByteBuffer(File file) throws IOException, TraineeException {
        ByteBuffer buffer = ByteBuffer.allocate(SIZE);
        try (FileChannel channel = new FileInputStream(file).getChannel()) {
            channel.read(buffer);
            String[] strings = new String(buffer.array(), "UTF-8").trim().split(" ");
            return new Trainee(strings[0], strings[1], Integer.parseInt(strings[2]));
        }
    }

    public static Trainee readFileBytesWithMappedByteBuffer(File file)throws IOException, TraineeException {
        try(FileChannel channel = new RandomAccessFile(file, "r").getChannel()){
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            byte[] array = ByteBuffer.allocate(buffer.capacity()).put(buffer).array();
            String[] strings = new String(array, "UTF-8").trim().split(" ");
            return new Trainee(strings[0], strings[1], Integer.parseInt(strings[2]));
        }
    }

    public static byte[] writeAndReadNumbersFrom0To99(File file) throws IOException {
        try(FileChannel channel = new RandomAccessFile(file, "rw").getChannel()){
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, SIZE);
            for(int i = 0; i < SIZE; i++){
                buffer.put((byte) (i));
            }
        }
        byte[] array = new byte[SIZE];
        try(FileInputStream fis = new FileInputStream(file)){
            fis.read(array);
        }
        return array;
    }

    public static Trainee getSerializedInstanceInByteBuffer(Trainee trainee) throws IOException, ClassNotFoundException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(outputStream)){
            oos.writeObject(trainee);
            byteBuffer.put(outputStream.toByteArray());
        }

        try(ByteArrayInputStream inputStream = new ByteArrayInputStream(byteBuffer.array());
            ObjectInputStream ois = new ObjectInputStream(inputStream)){
            return (Trainee) ois.readObject();
        }
    }

    public static void renameFromDatToBin(Path directory) throws IOException {
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                if (path.toString().endsWith(".dat")) {
                    path.toFile().renameTo(new File(path.toString().replace(".dat", ".bin")));
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

}