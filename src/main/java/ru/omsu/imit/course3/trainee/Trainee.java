package ru.omsu.imit.course3.trainee;

import java.io.Serializable;

public class Trainee implements Serializable, Comparable {

    private String name;
    private String surname;
    int mark;

    public Trainee(String name, String surname, int mark) throws TraineeException {
        this.setName(name);
        this.setSurname(surname);
        this.setMark(mark);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getMark() {
        return mark;
    }

    public void setName(String name) throws TraineeException{
        if(name == null || name.length() == 0){
            throw new TraineeException(TraineeErrorCodes.INCORRECT_NAME.getErrorString());
        }
        this.name = name;
    }

    public void setSurname(String surname) throws TraineeException{
        if(surname == null || surname.length() == 0){
            throw new TraineeException(TraineeErrorCodes.INCORRECT_SURNAME.getErrorString());
        }
        this.surname = surname;
    }

    public void setMark(int mark) throws TraineeException{
        if(mark<1 || mark>5){
            throw new TraineeException(TraineeErrorCodes.INCORRECT_MARK.getErrorString());
        }
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        return mark == trainee.mark && name.equals(trainee.name) && surname.equals(trainee.surname);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + mark;
        return result;
    }

    @Override
    public int compareTo(Object o) {
        if (this == o) return 1;
        if (o == null || getClass() != o.getClass()) return 0;

        Trainee trainee = (Trainee) o;
        if (name.equals(trainee.name)) return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "Trainee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mark=" + mark +
                '}';
    }
}