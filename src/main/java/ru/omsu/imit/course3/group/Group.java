package ru.omsu.imit.course3.group;

import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Group {
    private String name;
    private List<Trainee> trainees = Collections.synchronizedList(new ArrayList<>());

    public Group(String name, List<Trainee> trainees) throws GroupException, TraineeException {
        this.setName(name);
        this.setTrainees(trainees);
    }

    public String getName(){
        return name;
    }

    public void setName(String name) throws GroupException {
        if(name == null || name.length() == 0){
            throw new GroupException(GroupErrorCodes.INCORRECT_NAME.getErrorString());
        }
        this.name = name;
    }

    public List<Trainee> getTrainees() {
        return trainees;
    }

    public void setTrainees(List<Trainee> trainees) throws GroupException, TraineeException {
        if(trainees == null || trainees.size() == 0){
            throw new GroupException(GroupErrorCodes.INCORRECT_TRAINEES.getErrorString());
        }
        this.trainees = new LinkedList<>();
        for(Trainee trainee : trainees) {
            this.trainees.add(new Trainee(trainee.getName(), trainee.getSurname(), trainee.getMark()));
        }
    }
}
