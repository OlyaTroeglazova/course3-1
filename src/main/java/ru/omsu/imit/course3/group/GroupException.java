package ru.omsu.imit.course3.group;

public class GroupException extends Exception {
    public GroupException(){
        super();
    }

    public GroupException(String message){
        super(message);
    }
}
