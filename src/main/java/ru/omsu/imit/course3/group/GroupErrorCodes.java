package ru.omsu.imit.course3.group;

public enum  GroupErrorCodes {
    INCORRECT_NAME("Incorrect name"),
    INCORRECT_TRAINEES("Incorrect trainees");

    private final String errorString;

    private GroupErrorCodes(String errorString){
        this.errorString = errorString;
    }

    public String getErrorString(){
        return errorString;
    }
}
