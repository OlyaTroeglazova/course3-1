package ru.omsu.imit.course3;

import ru.omsu.imit.course3.person.Person;
import ru.omsu.imit.course3.person.PersonWithAge;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FunctionsDemo {
    public static final Function<String, List<?>> split = s -> s==null ? null : Arrays.asList(s.split(" "));

    public static final Function<List<?>, Integer> count = list -> list.size();

    public static final MyFunction<String, Integer> splitAndCount1 = s -> count.compose(split).apply(s);

    public static final MyFunction<String, Integer> splitAndCount2 = s -> split.andThen(count).apply(s);

    public static final MyFunction<String, List<?>> splitMethodReference = FunctionsDemo.split::apply;

    public static final MyFunction<List<?>, Integer> countMethodReference = FunctionsDemo.count::apply;

    public static final MyFunction<String, Person> create = s -> s==null ? null : new Person(s);

    public static final MyFunction<String, Person> createMethodReference = FunctionsDemo.create::apply;

    public static final BiFunction<Integer, Integer, Integer> max = Math::max;

    public static final Supplier<Date> getCurrentDate = () -> new Date();

    public static final Predicate<Integer> isEven = a -> a!=0 && a % 2 == 0;

    public static final BiPredicate<Integer, Integer> areEqual = (a, b) -> (a != null && b != null) && a == b;

    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op).sequential();
    }

    public static IntStream transformParall(IntStream stream, IntUnaryOperator op) {
        return stream.map(op).parallel();
    }

    public static final Function<List<PersonWithAge>, List<String>> getNamesUpper30SortByNameLength = persons -> persons.stream()
                .filter(person -> person.getAge() > 30)
                .map(PersonWithAge::getName)
                .distinct()
                .sorted((o1, o2) -> o2.length() - o1.length())
                .collect(Collectors.toList());

    public static final Function<List<PersonWithAge>, List<String>> getNamesUpper30SortByCountOfSimilar = persons -> persons.stream()
                .filter(person -> person != null && person.getAge() > 30)
                .map(PersonWithAge::getName)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
                .sorted(((o1, o2) -> (int) (o2.getValue() - o1.getValue())))
                .map(item -> item.getKey())
                .distinct()
                .collect(Collectors.toList());

    public static final Function<List<Integer>, Integer> sum = list->list.stream().reduce(0,(x, y)->x+y);

    public static final Function<List<Integer>, Integer> product = list->list.stream().reduce(1,(x, y)->x*y);
}
