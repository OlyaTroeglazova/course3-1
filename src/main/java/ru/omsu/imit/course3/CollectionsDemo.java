package ru.omsu.imit.course3;

import ru.omsu.imit.course3.group.Group;
import ru.omsu.imit.course3.group.GroupException;
import ru.omsu.imit.course3.institute.Institute;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.util.*;

public class CollectionsDemo {

    public static void sortingGroupMembersByMark(Group group) throws TraineeException, GroupException {
        Collections.sort(group.getTrainees(), (Trainee o1,Trainee o2)-> o1.getMark()-o2.getMark());
    }

    public static void sortingGroupMembersByName(Group group) throws TraineeException, GroupException {
        Collections.sort(group.getTrainees(), (Trainee o1,Trainee o2)-> o1.getName().compareTo(o2.getName()));
    }

    public static Trainee findTraineeInGroupByName(Group group, String name) throws TraineeException {
        for (Trainee trainee : group.getTrainees()) {
            if (trainee.getName().equals(name)) {
                return trainee;
            }
        }
        return null;
    }

    public static Trainee searchTraineeWithMaxMark(List<Trainee> trainees) {
        return Collections.max(trainees, (Trainee o1,Trainee o2)-> o1.getMark()-o2.getMark());
    }

    public static Trainee findTraineeByName(List<Trainee> trainees, String name) throws TraineeException {
        for (Trainee trainee : trainees) {
            if (trainee.getName().equals(name)) {
                return trainee;
            }
        }
        return null;
    }

    public static void printSet(Set<Trainee> traineeSet) {
        for (Trainee trainee : traineeSet) {
            System.out.println(trainee.toString());
        }
    }

    public static void printMap(Map<Trainee, Institute> map) {
        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey().toString() + entry.getValue().toString());
        }
    }

    public static void printTraineesInMap (Map<Trainee, Institute> map) {
        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey().toString());
        }
    }

    public static long measureTimeToFindInList(List<Integer> list){
        Random rnd = new Random();
        long start = System.nanoTime();
        for(int i = 0; i<100000; i++){
            list.get(rnd.nextInt(10000));
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long measureTimeToFindInSet(Set<Integer> set){
        Random rnd = new Random();
        long start = System.nanoTime();
        for(int i = 0; i<100000; i++){
            set.contains(rnd.nextInt(10000));
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long measureTimeToAddToBitSet(BitSet bitSet){
        long start = System.nanoTime();
        for(int i = 0; i<1000000; i++){
            bitSet.set(i);
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long measureTimeToAddToSet(Set<Integer> set){
        long start = System.nanoTime();
        for(int i = 0; i<1000000; i++){
            set.add(i);
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static Set<List<Integer>> getNonSimilarRows(List<List> matrix) {
        Set<List<Integer>> nonSimilarRows = new HashSet<>();
        nonSimilarRows.add(matrix.get(0));
        List<Integer> arrayList = matrix.get(0);
        for(List<Integer> list: matrix){
            for(int i: arrayList){
                if(!list.contains(i)){
                    nonSimilarRows.add(list);
                }
            }
        }
        return nonSimilarRows;
    }

    enum Color {
        RED, GREEN, BLUE, BLACK, WHITE
    }
}